const path = require('path');
const express = require('express');
const app = express();

// settings
app.set('port', process.env.PORT || 3000);

// static files
app.use(express.static(path.join(__dirname, 'public')));

// start server
const server = app.listen(app.get('port'), () => {
    console.log('server on port', app.get('port'));    
});

// websckets
const SocketIO = require('socket.io');
const io = SocketIO.listen(server);

io.on('connection', (socket) => {
    console.log('new connection', socket.id);

    socket.on('server:test', (data) => {
        console.log(data);
        socket.broadcast.emit('client:broadcast', data);
    });
});